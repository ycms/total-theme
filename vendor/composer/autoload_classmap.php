<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'YCMS\\Total\\Database\\Seeders\\TotalDatabaseSeeder' => $baseDir . '/Database/Seeders/TotalDatabaseSeeder.php',
    'YCMS\\Total\\Http\\Controllers\\TotalController' => $baseDir . '/Http/Controllers/TotalController.php',
    'YCMS\\Total\\Providers\\TotalServiceProvider' => $baseDir . '/Providers/TotalServiceProvider.php',
);
