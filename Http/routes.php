<?php

Route::group(['prefix' => 'total', 'namespace' => 'YCMS\Total\Http\Controllers'], function()
{
	Route::get('/', 'TotalController@index');
});