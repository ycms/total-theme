<?php namespace YCMS\Total\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class TotalController extends Controller {
	
	public function index()
	{
		return view('total::index');
	}
	
}